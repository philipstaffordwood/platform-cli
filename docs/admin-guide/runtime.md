#### Production Runtime

* **Docker Registry** (Harbor)
* **Certificate Management** (Cert-Manager)
* **Secret Management** (Sealed Secrets, Vault)
* **Monitoring** (Grafana, Prometheus, Thanos)
* **Logging** (ELK)
* **Authentication** (Dex)
* **Authorization & Policy Enforcement** (OPA)
* **Multi-Tenancy** (Namespace Configurator, Cluster Quotas)
* **Database as a Service** (Postgres)